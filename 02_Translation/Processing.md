content.js 是每次当浏览器网页加载页面的时候，都会调用的脚本，它可以作为监听页面事件的程序。

background.js 浏览器背后一直在运行的脚本，每次运行插件的时候，这个脚本就在后台持续运行，可以作为中介传输后台数据。

pop.js (可选) 是作为popup.html操作所引入的js库。


开发流程：

1.首先在content.js中捕捉到鼠标触发的事件。

2.将content.js中捕捉的鼠标触发事件存放到background.js事件中，让background.js作为数据存储的中心。

3.最后，用pop.js来引入background.js中的数据，让数据能够直接在popup.html中显示。