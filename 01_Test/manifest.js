{
    //json版本
    "manifest_version":2,
    //名称
    "name":"小小世界",
    //版本
    "version":"1.0.1.3",
    //描述
    "description":"这是我的第一个Chrome 插件，你想要试一下吗",
    //背景（这里会在Chrome的扩展后台显示Background.js）
    "background":{
        "scripts":["background.js"],
        "persistent":false
    },
    //这个东西会在后台运行
    "content_scripts":[
        {
            "matches":["<all_urls>"],
            "js":"content.js",
        }
    ]
    //权限（这里是指存储的权限）
    "permissions":["storage"],
    //icons
    "icons":{
       
        "128":"icon128.png",
        "48":"icon48.png",
        "16":"icon16.png"
    },

    "browser_action":{
   
        "default_icon":"icon16.png",
       "default_title":"小小世界",
        "default_popup":"popup.html"
    }

}


 // "content_scripts":[
    //     {
    //         "matches":["<all_urls>"],
    //         "js":["content.js","jquery.js"],
    //     }
    // ]
